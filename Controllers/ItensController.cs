using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Data;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ItensController : ControllerBase
    {
        private readonly DataContext _context;

        public ItensController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Item>> Get()
        {
            var itens = _context.Itens.ToList();
            return Ok(itens);
        }

        [HttpGet("{id}")]
        public ActionResult<Item> GetById(int id)
        {
            var item = _context.Itens.FirstOrDefault(i => i.Id == id);

            if(item == null)
                return NotFound();

            return Ok(item);
        }

        [HttpPost]
        public ActionResult<Item> Create(Item item)
        {
            var itemBanco = _context.Itens.Add(item);

            _context.SaveChanges();

            return Ok(new { msg = "Item cadastrado" });
        }
    }
}