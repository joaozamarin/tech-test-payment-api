using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Data;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly DataContext _context;

        public VendasController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Venda>> Get()
        {
            var vendas = _context.Vendas
                        .Include(v => v.Vendedor)
                        .Include(v => v.Itens)
                        .ToList();

            return Ok(vendas);
        }

        [HttpGet("{id}")]
        public ActionResult<IEnumerable<Venda>> GetById(int id)
        {
            var venda = _context.Vendas
                        .FirstOrDefault(v => v.Id == id);

            return Ok(venda);
        }

        [HttpPost]
        public ActionResult<Venda> Create(Venda venda)
        {
            var vendaBanco = _context.Vendas.Add(venda);

            _context.SaveChanges();

            return Ok(new { msg = "Venda cadastrada com sucesso" });
        }

        [HttpPut]
        public ActionResult<Venda> Update(int id, EnumStatusTarefa status)
        {
            var venda = _context.Vendas
                        .Include( v => v.Vendedor)
                        .Include( v => v.Itens)
                        .FirstOrDefault(v => v.Id == id);

            if(venda == null)
                return BadRequest();
            
            venda.Status = status.ToString();
            
            _context.SaveChanges();

            return Ok(venda);
        }
    }
}