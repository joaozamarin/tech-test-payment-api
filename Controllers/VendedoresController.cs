using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Data;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VendedoresController : ControllerBase
    {
        private readonly DataContext _context;

        public VendedoresController(DataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Vendedor>> Get()
        {
            var vendedores = _context.Vendedores.ToList();

            return Ok(vendedores);
        }

        [HttpPost]
        public ActionResult<Vendedor> Create(Vendedor vendedor)
        {
            var vendBanco = _context.Vendedores.Add(vendedor);

            _context.SaveChanges();

            return Ok(new {msg = "Vendedor criado com sucesso!"});
        }
    }
}