﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace techtestpaymentapi.Data.Migrations
{
    /// <inheritdoc />
    public partial class RemodelandoDados : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ItemVenda");

            migrationBuilder.DropTable(
                name: "ItensVendas");

            migrationBuilder.AddColumn<int>(
                name: "VendaId",
                table: "Itens",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Itens_VendaId",
                table: "Itens",
                column: "VendaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens",
                column: "VendaId",
                principalTable: "Vendas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens");

            migrationBuilder.DropIndex(
                name: "IX_Itens_VendaId",
                table: "Itens");

            migrationBuilder.DropColumn(
                name: "VendaId",
                table: "Itens");

            migrationBuilder.CreateTable(
                name: "ItemVenda",
                columns: table => new
                {
                    ItensId = table.Column<int>(type: "INTEGER", nullable: false),
                    VendasId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemVenda", x => new { x.ItensId, x.VendasId });
                    table.ForeignKey(
                        name: "FK_ItemVenda_Itens_ItensId",
                        column: x => x.ItensId,
                        principalTable: "Itens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ItemVenda_Vendas_VendasId",
                        column: x => x.VendasId,
                        principalTable: "Vendas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ItensVendas",
                columns: table => new
                {
                    VendaId = table.Column<int>(type: "INTEGER", nullable: false),
                    ItemId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItensVendas", x => new { x.VendaId, x.ItemId });
                    table.ForeignKey(
                        name: "FK_ItensVendas_Itens_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Itens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ItensVendas_Vendas_VendaId",
                        column: x => x.VendaId,
                        principalTable: "Vendas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ItemVenda_VendasId",
                table: "ItemVenda",
                column: "VendasId");

            migrationBuilder.CreateIndex(
                name: "IX_ItensVendas_ItemId",
                table: "ItensVendas",
                column: "ItemId");
        }
    }
}
