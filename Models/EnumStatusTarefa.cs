namespace tech_test_payment_api.Models
{
    public enum EnumStatusTarefa
    {
        Aguardando_Pagamento,
        Pagamento_Aprovado,
        Enviado_Transportadora,
        Entregue,
        Cancelada
    }
}