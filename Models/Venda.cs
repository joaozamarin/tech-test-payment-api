using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public int Id { get; set; }
        public string Data { get; set; } = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
        public string Status { get; set; } = EnumStatusTarefa.Aguardando_Pagamento.ToString();
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Item> Itens { get; set; }
    }
}